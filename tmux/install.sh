#!/bin/sh

[ "$(uname -s)" != "Darwin" ] && exit 0

echo "Installing tmux..."
brew install tmux

echo "Installing powerline status bar..."
pip install powerline-status
