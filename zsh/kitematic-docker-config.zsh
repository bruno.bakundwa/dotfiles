#!/usr/bin/env zsh

# Set docker-machine config info, so CLI docker
# tool can connect to Kitematic's 'default' VM image
eval "$(docker-machine env default)"
