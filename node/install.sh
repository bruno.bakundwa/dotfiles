#!/bin/sh
[ "$(uname -s)" != "Darwin" ] && exit 0

echo "Installing nvm..."
brew install nvm

node_version=5.1.0

echo "Installing node.js v${node_version}..."
nvm install $node_version
nvm alias default $node_version

echo "Installing a few useful modules to start with..."
npm install -g < `cat ~/.dotfiles/node/initital-modules.txt`
