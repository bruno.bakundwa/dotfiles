#!/bin/sh
#
# Homebrew
#
# This installs some of the common dependencies needed (or at least desired)
# using Homebrew.
[ "$(uname -s)" != "Darwin" ] && exit 0

# Check for Homebrew
if test ! "$(which brew)"; then
  echo "  Installing Homebrew for you."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

brew update

# Start with the basics...
brew install zsh tmux python

# Install Cask
brew tap phinze/homebrew-cask || true
brew tap caskroom/versions || true
brew install brew-cask

# Set up some useful stuff
brew install grc coreutils the_silver_searcher htop-osx \
  imagemagick wget unrar ffmpeg gifsicle terminal-notifier
brew cask install iterm2 the-unarchiver appcleaner diffmerge

# virtualization (required for docker, for instance)
brew cask install virtualbox

# quick look plugins - https://github.com/sindresorhus/quick-look-plugins
brew cask install qlcolorcode qlstephen qlmarkdown quicklook-json \
  qlprettypatch quicklook-csv betterzipql qlimagesize webpquicklook \
  suspicious-package

# clean things up
brew cleanup
brew cask cleanup
