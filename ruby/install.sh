#!/bin/sh
[ "$(uname -s)" != "Darwin" ] && exit 0

if test ! "$(which rbenv)"; then
  echo "Installing rbenv"
  brew install rbenv > /tmp/rbenv-install.log
fi

if test ! "$(which ruby-build)"; then
  echo "Install ruby-build"
  brew install ruby-build > /tmp/ruby-build-install.log
fi

# Ruby version & initial gems to install
ruby_version=2.2.3
initial_gems=(pry bundler)

echo "Installing ruby v${ruby_version}..."
rbenv install $ruby_version --verbose
rbenv global $ruby_version
echo "Done. You're using ruby v${ruby_version}"

echo "Installing initial gems..."
for gem in $initial_gems; do
  gem install $gem
done
