#!/bin/sh
echo "Installing Atom editor"
[ "$(uname -s)" = "Darwin" ] && brew cask install atom

echo "Installing Atom packages"
apm install \
  minimap \
  editorconfig \
  run-command \
  vim-mode \
  ex-mode \
  vim-surround \
  jumpy \
  relative-numbers \
  language-docker \
  language-diff \
  language-puppet \
  language-terraform \
  linter \
  linter-jshint \
  linter-jscs \
  linter-ruby \
  atom-beautify \
  color-picker \
  go-plus \
  go-rename \
  language-tmux \
  git-plus \
  term3 \
  auto-detect-indentation \
  autoprefixer \
  dash \
  emmet \
  run-command \
  file-icons || true
